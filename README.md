# Rush for the Web Card Asset Generator

This code takes the export of a series of card CSVs from a spreadsheet found here:

https://docs.google.com/spreadsheets/d/1Gm4aHk2iisOXalIk8MB7SXHSUJ-mX18JrsNar-8obuI/edit

and generates card image sheets suitable for import into Tabletop Simulator. It also generates the `cards.json` file used by the mobile app from the spreadheet exports. The purpose is to create an effective pipeline for quickly updating cards and deploying them across digital tabletops, the backend and mobile app.

## Preconditions

This uses Imagemagick to perform image operations. You will need to install the dependencies on your machine to run this: 

```
sudo dnf install GraphicsMagick GraphicsMagick-devel
sudo dnf install /usr/bin/MagickWand-config
go get gopkg.in/gographics/imagick.v2/imagick
```

This code was engineered on Fedora and will probably not run on other operating systems.

## Running the Generator

The generator is run by a supplied script that generates intermediate card images and assembles them into sheets compliant to the Tabletop Simulator specs. To run the generator, copy the card CSVs into the local directory and issue:

```
./generate.sh
```

This generates seperate sheet PNGs for each of events, personas, projects and resources along with the `cards.json` file.
