#!/bin/bash

echo "generating rounded corner backs.."
convert cards/back-resource-src.png -matte cards/cornermask.png -compose DstIn -composite cards/back-resource.png
convert cards/back-project-src.png -matte cards/cornermask.png -compose DstIn -composite cards/back-.png
convert cards/back-persona-src.png -matte cards/cornermask.png -compose DstIn -composite cards/back-.png
convert cards/back-event-src.png -matte cards/cornermask.png -compose DstIn -composite cards/back-.png
echo "creating cards.."
./cardgenerator
echo "creating sheets.."
montage ./out-resources/resource-*.png -tile 10x7 -geometry 340x500! -background none sheet-resources.png
montage ./out-projects/project-*.png -tile 10x7 -geometry 340x500! -background none sheet-projects.png
montage ./out-personas/persona-*.png -tile 10x7 -geometry 340x500! -background none sheet-personas.png
montage ./out-events/event-*.png -tile 10x7 -geometry 340x500! -background none sheet-events.png
echo "creating pnp pdf.."
montage -density 300 -tile 3x3 -geometry +0+0 ./out-resources/resource-*.png pnp-resourcecards.pdf
montage -density 300 -tile 3x3 -geometry +0+0 ./out-projects/project-*.png pnp-projectcards.pdf
montage -density 300 -tile 3x3 -geometry +0+0 ./out-personas/persona-*.png pnp-personacards.pdf
montage -density 300 -tile 3x3 -geometry +0+0 ./out-events/event-*.png pnp-eventcards.pdf
#echo "cleaning up.."
#rm -rf ./out-resources/
#rm -rf ./out-projects/
#rm -rf ./out-personas/
#rm -rf ./out-events/
echo "done"

