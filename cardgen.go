/*
 * Launchpad Card Generator
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// NOTE: this code is more or less a hack to quickly generate card assets for digital tabletops.
// don't expect any sane code quality in here. It is merely a dirty shim.

package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strconv"
	"strings"

	"gopkg.in/gographics/imagick.v2/imagick"
)

var counter = 0

type JSONCard struct {
	ID             string      `json:"ID"`
	Type           string      `json:"type"`
	Name           []string    `json:"name"`
	Description    []string    `json:"description"`
	IllustrationID string      `json:"illustrationid"`
	IconID         string      `json:"iconid"`
	EffectID       string      `json:"effectid"`
	EffectArgs     []string    `json:"effectargs"`
	Colors         []string    `json:"colors"`
	Cost           int         `json:"cost"`
	Payout         map[int]int `json:"payout"`
}

func convColors(colorCode string) string {
	switch colorCode {
		case "a": return "#a42a22"
		case "b": return "#6ba869"
		case "c": return "#2b4967"
		case "d": return "#8c57a3"
		case "e": return "#d6bb38"
		case "f": return "#805f3e"
		case "g": return "#bcbcbc"
		default: return ""
	}
}

// breakText wraps a string into lines of a given length.
func breakText(text string, maxlen int) string {
	var lines []string
	var line string
	for _, word := range strings.Split(text, " ") {
		if len(line) == 0 {
			line = word
		} else if len(line) + len(word) + 1 <= maxlen {
			line += " " + word
		} else {
			lines = append(lines, line)
			line = word
		}
	}
	lines = append(lines, line)
	return strings.Join(lines, "\n")
}

func createProjectCard(title []string, text []string, colors []string, colorValues []string, sellTable string, illustrationID string, iconID string, filename string) (JSONCard, error) {
	var err error
	mw := imagick.NewMagickWand()
	dw := imagick.NewDrawingWand()
	pw := imagick.NewPixelWand()
	ow := imagick.NewMagickWand()
	ew := imagick.NewMagickWand()
	bw := imagick.NewPixelWand()
	
	// image assembly
	err = mw.ReadImage("cards/front-project.png")
	if err != nil {
		return JSONCard{}, err
	}
	err = ow.ReadImage("artworks/" + illustrationID + ".png")
	if err != nil {
		return JSONCard{}, err
	}
	ow.ScaleImage(450, 450)
	mw.CompositeImage(ow, imagick.COMPOSITE_OP_OVER, 92, 250)
	ow.Destroy()

	// title
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(0,0,0)")
	dw.SetFillColor(pw)
	dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
	dw.Annotation(25, 20, title[0])
	mw.DrawImage(dw)

	// colors
	if len(colors) != 3 {
		return JSONCard{}, errors.New("need 3 colors")
	}
	dw.SetStrokeColor(bw)
	dw.SetStrokeWidth(2)
	if colors[0] != "" {
		pw.SetColor(colorValues[0])
		dw.SetFillColor(pw)
		dw.Rectangle(600, 120, 635, 250)
	}
	if colors[1] != "" {
		pw.SetColor(colorValues[1])
		dw.SetFillColor(pw)
		dw.Rectangle(600, 260, 635, 390)
	}
	if colors[2] != "" {
		pw.SetColor(colorValues[2])
		dw.SetFillColor(pw)
		dw.Rectangle(600, 400, 635, 530)
	}
	mw.DrawImage(dw)

	// sell table
	var sellTableObj map[int]int
	err = json.Unmarshal([]byte(sellTable), &sellTableObj)
	if err != nil {
		return JSONCard{}, err
	}
	// 2 cards
	err = ew.ReadImage("icons/icon-cards-2.png")
	if err != nil {
		return JSONCard{}, err
	}
	ew.ScaleImage(uint(ICON_SIZE), uint(ICON_SIZE))
	mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, 15, 97)
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(255,255,255)")
	dw.SetFillColor(pw)
	dw.SetStrokeColor(pw)
	dw.SetStrokeWidth(0)
	dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
	dw.Annotation(80, 110, strconv.Itoa(sellTableObj[2]))
	mw.DrawImage(dw)

	// 3 cards
	err = ew.ReadImage("icons/icon-cards-3.png")
	if err != nil {
		return JSONCard{}, err
	}
	ew.ScaleImage(uint(ICON_SIZE), uint(ICON_SIZE))
	mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, 115, 97)
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(255,255,255)")
	dw.SetFillColor(pw)
	dw.SetStrokeColor(pw)
	dw.SetStrokeWidth(0)
	dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
	dw.Annotation(185, 110, strconv.Itoa(sellTableObj[3]))
	mw.DrawImage(dw)

	// 4 cards
	err = ew.ReadImage("icons/icon-cards-4.png")
	if err != nil {
		return JSONCard{}, err
	}
	ew.ScaleImage(uint(ICON_SIZE), uint(ICON_SIZE))
	mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, 220, 97)
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(255,255,255)")
	dw.SetFillColor(pw)
	dw.SetStrokeColor(pw)
	dw.SetStrokeWidth(0)
	dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
	dw.Annotation(300, 110, strconv.Itoa(sellTableObj[4]))
	mw.DrawImage(dw)

	// apply corner mask
	clipw := imagick.NewMagickWand()
	err = clipw.ReadImage("cards/cornermask.png")
	if err != nil {
		return JSONCard{}, err
	}
	mw.CompositeImage(clipw, imagick.COMPOSITE_OP_DST_IN, 0, 0)

	err = mw.WriteImages(filename, true)

	card := JSONCard{
		ID:             "" + strconv.Itoa(counter),
		Type:           "project",
		Name:           []string{title[0], title[1]},
		Description:    []string{text[0], text[1]},
		IllustrationID: illustrationID,
		IconID:         iconID,
		EffectID:       "",
		EffectArgs:     nil,
		Colors:         colors,
		Cost:           0,
		Payout:         sellTableObj,
	}
	counter++

	return card, err
}

const CARD_WIDTH int = 635
const CARD_HEIGHT int = 888
const ICON_SIZE int = 80

func renderEffects(mw *imagick.MagickWand, dw *imagick.DrawingWand, pw *imagick.PixelWand, effectID string, effectArgs string, yOffset int) error {
	var err error
	ew := imagick.NewMagickWand()
	bw := imagick.NewPixelWand()
	bw.SetColor("rgb(0,0,0)")
	if effectID != "" {
		err = ew.ReadImage("icons/" + effectID + ".png")
		if err != nil {
			return err
		}
		ew.ScaleImage(uint(ICON_SIZE), uint(ICON_SIZE))
		if effectID == "draw_additional_project" || effectID == "add_take_pool" || effectID == "add_take_resources" || effectID == "add_play_card" || effectID == "add_getpool_discard_pool" || effectID == "add_draw_discard_pool" {
			// single icon
			mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, (CARD_WIDTH - ICON_SIZE) / 2, 750 + yOffset)
		} else if effectID == "sell_bonus" {
			// icon with 1 arg
			var args []string
			err = json.Unmarshal([]byte(effectArgs), &args)
			if err != nil {
				return err
			}
			yPos := float64(CARD_HEIGHT) * 0.85 + float64(yOffset)
			xCenterCorrection := int(math.Round(float64(CARD_WIDTH)*float64(0.025)))
			mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, ((CARD_WIDTH - 2*ICON_SIZE) / 2) + xCenterCorrection, int(yPos))
			dw.SetFont("NotoSans-Bold")
			dw.SetFontSize(42)
			pw.SetColor("rgb(0,0,0)")
			dw.SetFillColor(pw)
			dw.SetGravity(imagick.GRAVITY_SOUTH)
			dw.Annotation((float64(ICON_SIZE)*0.25) + float64(xCenterCorrection), float64(CARD_HEIGHT)-yPos-(float64(ICON_SIZE)*0.9), args[0])
			mw.DrawImage(dw)
		} else if effectID == "credit_bonus" {
			// icon with 2 args
			var args []string
			err = json.Unmarshal([]byte(effectArgs), &args)
			if err != nil {
				return err
			}
			yPos := float64(CARD_HEIGHT) * 0.85 + float64(yOffset)
			xCenterCorrection := int(math.Round(float64(CARD_WIDTH)*float64(0.03)))
			mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, ((CARD_WIDTH - ICON_SIZE) / 2) + xCenterCorrection, int(yPos))
			dw.SetFont("NotoSans-Bold")
			dw.SetFontSize(42)
			pw.SetColor("rgb(0,0,0)")
			dw.SetFillColor(pw)
			dw.SetGravity(imagick.GRAVITY_SOUTH)
			dw.Annotation((float64(ICON_SIZE)*0.8) + float64(xCenterCorrection), float64(CARD_HEIGHT)-yPos-(float64(ICON_SIZE)*0.85), args[0])
			mw.DrawImage(dw)
			err = ew.ReadImage("icons/" + args[1] + ".png")
			if err != nil {
				return err
			}
			ew.ScaleImage(uint(ICON_SIZE), uint(ICON_SIZE))
			mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, 205, int(yPos))
		} else if effectID == "sell_bonus_color" {
			// icon with 2 or 3 args
			var args []string
			err = json.Unmarshal([]byte(effectArgs), &args)
			if err != nil {
				return err
			}
			if len(args) == 2 {
				yPos := float64(CARD_HEIGHT) * 0.85 + float64(yOffset)
				xCenterCorrection := int(math.Round(float64(CARD_WIDTH)*float64(0.045)))
				mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, ((CARD_WIDTH - 3*ICON_SIZE) / 2) + xCenterCorrection, int(yPos))
				dw.SetFont("NotoSans-Bold")
				dw.SetFontSize(32)
				pw.SetColor("rgb(0,0,0)")
				dw.SetFillColor(pw)
				dw.SetGravity(imagick.GRAVITY_SOUTH)
				dw.Annotation(-float64(ICON_SIZE) *0.3 + float64(xCenterCorrection), float64(CARD_HEIGHT)-yPos-(float64(ICON_SIZE)*0.77), args[0])
				mw.DrawImage(dw)
				pw.SetColor(convColors(args[1]))
				dw.SetFillColor(pw)
				dw.SetStrokeColor(bw)
				dw.SetStrokeWidth(3)
				dw.Ellipse(float64(1.9) * float64(ICON_SIZE) + float64(((CARD_WIDTH - 3*ICON_SIZE) / 2) + xCenterCorrection), yPos + float64(ICON_SIZE) / 2, 25, 25, 0, 360)
				mw.DrawImage(dw)
			} else if len(args) == 3 {
				yPos := float64(CARD_HEIGHT) * 0.85 + float64(yOffset)
				xCenterCorrection := int(math.Round(float64(CARD_WIDTH)*float64(0.025)))
				mw.CompositeImage(ew, imagick.COMPOSITE_OP_OVER, ((CARD_WIDTH - 3*ICON_SIZE) / 2) + xCenterCorrection, int(yPos))
				dw.SetFont("NotoSans-Bold")
				dw.SetFontSize(32)
				pw.SetColor("rgb(0,0,0)")
				dw.SetFillColor(pw)
				dw.SetGravity(imagick.GRAVITY_SOUTH)
				dw.Annotation(-float64(ICON_SIZE) *0.3 + float64(xCenterCorrection), float64(CARD_HEIGHT)-yPos-(float64(ICON_SIZE)*0.77), args[0])
				mw.DrawImage(dw)
				pw.SetColor(convColors(args[1]))
				dw.SetFillColor(pw)
				dw.SetStrokeColor(bw)
				dw.SetStrokeWidth(3)
				dw.Ellipse(float64(1.9) * float64(ICON_SIZE) + float64(((CARD_WIDTH - 3*ICON_SIZE) / 2) + xCenterCorrection), yPos + float64(ICON_SIZE) / 2, 25, 25, 0, 360)
				mw.DrawImage(dw)
				pw.SetColor(convColors(args[2]))
				dw.SetStrokeColor(bw)
				dw.SetStrokeWidth(3)
				dw.SetFillColor(pw)
				dw.Ellipse(float64(2.3) * float64(ICON_SIZE) + float64(((CARD_WIDTH - 3*ICON_SIZE) / 2) + xCenterCorrection), yPos + float64(ICON_SIZE) / 2, 25, 25, 0, 360)
				mw.DrawImage(dw)
			}
		}
		ew.Destroy()
	}
	return nil
}

func createResourceCard(title []string, text []string, playAndSell bool, costCredits int, color string, colorValue string, artworkImage string, effectID string, effectArgs string, illustrationID string, iconID string, filename string) (JSONCard, error) {
	var err error
	mw := imagick.NewMagickWand()
	dw := imagick.NewDrawingWand()
	pw := imagick.NewPixelWand()
	ow := imagick.NewMagickWand()
	cw := imagick.NewMagickWand()

	// image assembly
	err = mw.ReadImage("cards/front-resource-" + colorValue + ".png")
	if err != nil {
		return JSONCard{}, err
	}
	err = ow.ReadImage(artworkImage)
	if err != nil {
		return JSONCard{}, err
	}
	ow.ScaleImage(450, 450)
	if effectID == "" {
		mw.CompositeImage(ow, imagick.COMPOSITE_OP_OVER, 92, 225)
	} else {
		mw.CompositeImage(ow, imagick.COMPOSITE_OP_OVER, 92, 200)
	}
	ow.Destroy()

	// credits
	err = cw.ReadImage("icons/icon-credits.png")
	if err != nil {
		return JSONCard{}, err
	}
	cw.ScaleImage(uint(ICON_SIZE), uint(ICON_SIZE))
	if costCredits > 2 {
		mw.CompositeImage(cw, imagick.COMPOSITE_OP_OVER, 455, 60)
	}
	if costCredits > 1 {
		mw.CompositeImage(cw, imagick.COMPOSITE_OP_OVER, 480, 60)
	}
	mw.CompositeImage(cw, imagick.COMPOSITE_OP_OVER, 505, 60)
	cw.Destroy()

	// effects
	err = renderEffects(mw, dw, pw, effectID, effectArgs, 0)
	if err != nil {
		return JSONCard{}, err
	}

	// title
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(0,0,0)")
	dw.SetFillColor(pw)
	dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
	dw.Annotation(20, 20, breakText(title[0], 25))
	mw.DrawImage(dw)

	// text
	dw.SetFont("NotoSans-Regular")
	descText := ""
	if playAndSell {
		dw.SetFontSize(14)
		descText = breakText(text[0], 32)
	} else {
		dw.SetFontSize(18)
		descText = breakText(text[0], 23)
	}
	pw.SetColor("rgb(0,0,0)")
	dw.SetFillColor(pw)
	dw.SetGravity(imagick.GRAVITY_CENTER)
	dw.Annotation(0, 110, descText)
	mw.DrawImage(dw)

	// apply corner mask
	clipw := imagick.NewMagickWand()
	err = clipw.ReadImage("cards/cornermask.png")
	if err != nil {
		return JSONCard{}, err
	}
	mw.CompositeImage(clipw, imagick.COMPOSITE_OP_DST_IN, 0, 0)

	err = mw.WriteImages(filename, true)

	var effectArgsObj []string
	json.Unmarshal([]byte(effectArgs), &effectArgsObj)
	card := JSONCard{
		ID:             "" + strconv.Itoa(counter),
		Type:           "resource",
		Name:           []string{title[0], title[1]},
		Description:    []string{text[0], text[1]},
		IllustrationID: illustrationID,
		IconID:         iconID,
		EffectID:       effectID,
		EffectArgs:     effectArgsObj,
		Colors:         []string{color},
		Cost:           costCredits,
		Payout:         map[int]int{},
	}
	counter++

	return card, err
}

func createPersonaCard(title []string, text []string, personaImage string, effectID string, effectArgs string, illustrationID string, iconID string, filename string) (JSONCard, error) {
	var err error
	mw := imagick.NewMagickWand()
	dw := imagick.NewDrawingWand()
	pw := imagick.NewPixelWand()

	// image assembly
	err = mw.ReadImage(personaImage)
	if err != nil {
		return JSONCard{}, err
	}

	// title
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(0,0,0)")
	dw.SetFillColor(pw)
	dw.SetGravity(imagick.GRAVITY_NORTH)
	dw.Annotation(0, 19, breakText(title[0], 25))
	mw.DrawImage(dw)

	// effects
	err = renderEffects(mw, dw, pw, effectID, effectArgs, 5)
	if err != nil {
		return JSONCard{}, err
	}

	// apply corner mask
	clipw := imagick.NewMagickWand()
	err = clipw.ReadImage("cards/cornermask.png")
	if err != nil {
		return JSONCard{}, err
	}
	mw.CompositeImage(clipw, imagick.COMPOSITE_OP_DST_IN, 0, 0)

	err = mw.WriteImages(filename, true)

	var effectArgsObj []string 
	json.Unmarshal([]byte(effectArgs), &effectArgsObj)
	card := JSONCard{
		ID:             "" + strconv.Itoa(counter),
		Type:           "persona",
		Name:           []string{title[0], title[1]},
		Description:    []string{text[0], text[1]},
		IllustrationID: illustrationID,
		IconID:         iconID,
		EffectID:       effectID,
		EffectArgs:     effectArgsObj,
		Colors:         []string{},
		Cost:           0,
		Payout:         map[int]int{},
	}
	counter++

	return card, err
}

func createEventCard(title []string, text []string, eventImage string, effectID string, effectArgs string, illustrationID string, iconID string, filename string) (JSONCard, error) {
	var err error
	mw := imagick.NewMagickWand()
	dw := imagick.NewDrawingWand()
	pw := imagick.NewPixelWand()
	ow := imagick.NewMagickWand()

	// image assembly
	err = mw.ReadImage("cards/front-event.png")
	if err != nil {
		return JSONCard{}, err
	}
	err = ow.ReadImage(eventImage)
	if err != nil {
		return JSONCard{}, err
	}
	ow.ScaleImage(450, 450)
	if effectID == "" {
		mw.CompositeImage(ow, imagick.COMPOSITE_OP_OVER, 92, 245)
	} else {
		mw.CompositeImage(ow, imagick.COMPOSITE_OP_OVER, 92, 190)
	}
	ow.Destroy()

	// title
	dw.SetFont("NotoSans-Bold")
	dw.SetFontSize(42)
	pw.SetColor("rgb(0,0,0)")
	dw.SetFillColor(pw)
	dw.SetGravity(imagick.GRAVITY_NORTH)
	dw.Annotation(0, 20, breakText(title[0], 20))
	mw.DrawImage(dw)

	// effects
	err = renderEffects(mw, dw, pw, effectID, effectArgs, 25)
	if err != nil {
		return JSONCard{}, err
	}

	// apply corner mask
	clipw := imagick.NewMagickWand()
	err = clipw.ReadImage("cards/cornermask.png")
	if err != nil {
		return JSONCard{}, err
	}
	mw.CompositeImage(clipw, imagick.COMPOSITE_OP_DST_IN, 0, 0)

	err = mw.WriteImages(filename, true)

	var effectArgsObj []string
	json.Unmarshal([]byte(effectArgs), &effectArgsObj)
	card := JSONCard{
		ID:             "" + strconv.Itoa(counter),
		Type:           "event",
		Name:           []string{title[0], title[1]},
		Description:    []string{text[0], text[1]},
		IllustrationID: illustrationID,
		IconID:         iconID,
		EffectID:       effectID,
		EffectArgs:     effectArgsObj,
		Colors:         []string{},
		Cost:           0,
		Payout:         map[int]int{},
	}
	counter++

	return card, err
}

func readCSV(csvFile string) ([][]string, error) {
	var err error
	var csvRows [][]string

	csvfile, err := os.Open(csvFile)
	if err != nil {
		return nil, err
	}
	csvReader := csv.NewReader(csvfile)
	csvRows, err = csvReader.ReadAll()
	if err != nil {
		return nil, err
	}

	return csvRows, nil
}

func createResourceCards(directory string) ([]JSONCard, error) {
	resourceCardData, err := readCSV("Launchpad Cards - Resources Cards.csv")
	if err != nil {
		panic(err)
	}
	cards := []JSONCard{}
	for idx, row := range resourceCardData {
		if idx > 0 {
			fmt.Println("creating resource card " + row[1])
			artworkFile := "artworks/" + row[9] + ".png"
			textEN := ""
			if row[5] != "" {
				textEN += row[5]
			}
			textDE := ""
			if row[6] != "" {
				textDE += row[6]
			}
			filename := directory + "/resource-" + strconv.Itoa(idx) + ".png"
			costCredits, err := strconv.Atoi(row[4])
			if err != nil {
				return nil, err
			}
			card, err := createResourceCard([]string{row[1], row[2]}, []string{textEN, textDE}, false, costCredits, row[3], convColors(row[3]), artworkFile, row[7], row[8], row[9], row[10], filename)
			cards = append(cards, card)
			if err != nil {
				return nil, err
			}
		}
	}
	return cards, nil
}

func createProjectCards(directory string) ([]JSONCard, error) {
	projectCardData, err := readCSV("Launchpad Cards - Project Cards.csv")
	if err != nil {
		panic(err)
	}
	cards := []JSONCard{}
	for idx, row := range projectCardData {
		if idx > 0 {
			fmt.Println("creating project card " + row[1])
			filename := directory + "/project-" + strconv.Itoa(idx) + ".png"
			card, err := createProjectCard([]string{row[1], row[2]}, []string{row[3], row[4]}, []string{row[5], row[6], row[7]}, []string{convColors(row[5]), convColors(row[6]), convColors(row[7])}, row[8], row[10], row[11], filename)
			if err != nil {
				return nil, err
			}
			cards = append(cards, card)
		}
	}
	return cards, nil
}

func createPersonaCards(directory string) ([]JSONCard, error) {
	personaCardData, err := readCSV("Launchpad Cards - Persona Cards.csv")
	if err != nil {
		panic(err)
	}
	cards := []JSONCard{}
	for idx, row := range personaCardData {
		if idx > 0 {
			fmt.Println("creating persona card " + row[1])
			filename := directory + "/persona-" + strconv.Itoa(idx) + ".png"
			var personaImageFile string
			switch row[0] {
				case "1": personaImageFile = "cards/front-persona-filmmaker.png"
				case "2": personaImageFile = "cards/front-persona-musician.png"
				case "3": personaImageFile = "cards/front-persona-podcaster.png"
				case "4": personaImageFile = "cards/front-persona-gamer.png"
				case "5": personaImageFile = "cards/front-persona-blogger.png"
				case "6": personaImageFile = "cards/front-persona-writer.png"
			}
			card, err := createPersonaCard([]string{row[1], row[2]}, []string{row[5], row[6]}, personaImageFile, row[3], row[4], row[7], row[8], filename)
			if err != nil {
				return nil, err
			}
			cards = append(cards, card)
		}
	}
	return cards, nil
}

func createEventCards(directory string) ([]JSONCard, error) {
	eventCardData, err := readCSV("Launchpad Cards - Event Cards.csv")
	if err != nil {
		panic(err)
	}
	cards := []JSONCard{}
	for idx, row := range eventCardData {
		if idx > 0 {
			fmt.Println("creating event card " + row[1])
			filename := directory + "/event-" + strconv.Itoa(idx) + ".png"
			eventImageFile := "artworks/" + row[7] + ".png"
			card, err := createEventCard([]string{row[1], row[2]}, []string{row[5], row[6]}, eventImageFile, row[3], row[4], row[7], row[8], filename)
			if err != nil {
				return nil, err
			}
			cards = append(cards, card)
		}
	}
	return cards, nil
}

func main() {
	imagick.Initialize()
	defer imagick.Terminate()

	cards := []JSONCard{}

	os.Mkdir("./out-resources", 0755)
	rCards, err := createResourceCards("./out-resources")
	cards = append(cards, rCards...)
	if err != nil {
		panic(err)
	}

	os.Mkdir("./out-personas", 0755)
	sCards, err := createPersonaCards("./out-personas")
	cards = append(cards, sCards...)
	if err != nil {
		panic(err)
	}

	os.Mkdir("./out-events", 0755)
	eCards, err := createEventCards("./out-events")
	cards = append(cards, eCards...)
	if err != nil {
		panic(err)
	}

	os.Mkdir("./out-projects", 0755)
	pCards, err := createProjectCards("./out-projects")
	cards = append(cards, pCards...)
	if err != nil {
		panic(err)
	}

	file, _ := json.MarshalIndent(cards, "", " ")
	_ = ioutil.WriteFile("cards.json", file, 0644)
}